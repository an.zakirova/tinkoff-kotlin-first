package intensive_kotlin_advanced_part2.homework2_7

val sum = { a: Double, b: Double -> a + b }
val subtraction = { a: Double, b: Double -> a - b }
val multiplication = { a: Double, b: Double -> a * b }
val division = { a: Double, b: Double -> a / b }


fun main() {
    calculator("+", 2.0, 3.0)
    calculator("-", 2.0, 13.0)
    calculator("*", 5.0, 5.8)
    calculator("/", 15.0, 5.0)
}

fun calculator(operator: String, a: Double, b: Double) {
    val lambda = getCalculationMethod(operator)
    println("$a $operator $b = ${lambda(a, b)}")
}

fun getCalculationMethod(name: String): (Double, Double) -> Double {
    return when (name) {
        "+" -> sum
        "-" -> subtraction
        "*" -> multiplication
        "/" -> division
        else -> throw UnsupportedOperationException()
    }
}