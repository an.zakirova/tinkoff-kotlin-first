package intensive_kotlin_advanced_part2.homework2_2

fun main() {
    val list = listOf(1.0, 3.877, 2.99, 6.0, null)

    println("Искомая сумма элементов в оставшейся коллекции: ${"%.2f".format(performCollectionOperations(list))}")
}

fun performCollectionOperations(list: List<Double?>): Double {

    val newList = list.mapNotNull {
        if (it == null) {
            return@mapNotNull null
        } else if (it.toInt() % 2 == 0) {
            return@mapNotNull it * it
        } else {
            return@mapNotNull it / 2
        }
    }.filter { return@filter it < 25 }.sortedDescending().take(10).sum()
    return newList
}