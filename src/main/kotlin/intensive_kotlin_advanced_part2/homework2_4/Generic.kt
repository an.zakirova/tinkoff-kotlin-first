package intensive_kotlin_advanced_part2.homework2_4

fun main() {
    val lion = Lion()
    useRunSkill(lion)
    useSwimAndRunSkill(lion)
    useSwimSkill(lion)
    val tiger = Tiger()
    useRunSkill(tiger)
    useSwimAndRunSkill(tiger)
    useSwimSkill(tiger)
    val salmon = Salmon()
    useSwimSkill(salmon)
    val tuna = Tuna()
    useSwimSkill(tuna)
}

abstract class Pet

interface Runnable {
    fun run()
}

interface Swimmable {
    fun swim()
}

open class Cat : Pet(), Runnable, Swimmable {
    override fun run() {
        println("I am a Cat, and i running")
    }

    override fun swim() {
        println("I am a Cat, and i swimming")
    }
}

open class Fish : Pet(), Swimmable {

    override fun swim() {
        println("I am a Fish, and i swimming")
    }
}

class Lion() : Cat() {
    override fun run() {
        println("I am a Lion, and i running")
    }

    override fun swim() {
        println("I am a Lion, and i swimming")
    }
}

class Tiger : Cat() {
    override fun run() {
        println("I am a Tiger, and i running")
    }

    override fun swim() {
        println("I am a Tiger, and i swimming")
    }
}

class Salmon : Fish() {
    override fun swim() {
        println("I am a Salmon, and i swimming")
    }
}

class Tuna : Fish() {
    override fun swim() {
        println("I am a Tuna, and i swimming")
    }
}

fun <T : Runnable> useRunSkill(pet: T) {
    pet.run()
}

fun <T : Swimmable> useSwimSkill(fish: T) {
    fish.swim()
}

fun <T> useSwimAndRunSkill(animal: T) where T : Swimmable, T : Runnable {
    animal.run()
    animal.swim()
}

