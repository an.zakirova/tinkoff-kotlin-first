package intensive_kotlin_advanced_part2.homework2_8

fun main() {
    val inn = "-07574944998"
    validateInnFunction(inn, validateInn)
}
fun validateInnFunction(inn: String, function: (String) -> Boolean) {
    println("ИНН $inn ${if (function(inn)) "валиден" else "не валиден"}")
}
val validateInn = validateInn@{ inn: String ->
    try {
        if (inn.length != 12) {
            return@validateInn false
        }
        if (inn.toLongOrNull() == null || inn.toLong() < 0) {
            return@validateInn false
        }
    } catch (e: Exception) {
        return@validateInn false
    }
    val checkSumCoeff1 = listOf(7, 2, 4, 10, 3, 5, 9, 4, 6, 8)
    val checkSumCoeff2 = listOf(3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8)
    val listInn = inn.map { it.digitToInt() }
    val checkSum1 = (listInn.take(10).zip(checkSumCoeff1) { num, coef -> num * coef }.sum() % 11) % 10
    val checkSum2 = (listInn.take(11).zip(checkSumCoeff2) { num, coef -> num * coef }.sum() % 11) % 10
    checkSum1 == listInn[10] && checkSum2 == listInn[11]
}

