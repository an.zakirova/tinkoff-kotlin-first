package course_kotlin.homework6

fun main() {
    // Задание 1
    val lion = Lion("Alex", 1, 190)
    lion.eat("zebra")
    println("Сытость льва ${lion.satiety}")

    val tiger = Tiger("Rex", 1, 150)
    tiger.eat("boar")
    println("Сытость тигра ${tiger.satiety}")

    val hippopotamus = Hippopotamus("Gloria", 2, 1400)
    hippopotamus.eat("vegetables")
    println("Сытость бегемота ${hippopotamus.satiety}")

    val wolf = Wolf("Rob", 1, 150)
    wolf.eat("elk")
    println("Сытость волка ${wolf.satiety}")

    val giraffe = Giraffe("Melman", 6, 1200)
    giraffe.eat("leaves")
    println("Сытость жирафа ${giraffe.satiety}")

    val elephant = Elephant("Lee", 3, 5000)
    elephant.eat("grass")
    println("Сытость слона ${elephant.satiety}")

    val chimpanzee = Chimpanzee("Moris", 1, 70)
    chimpanzee.eat("fruits")
    println("Сытость шимпанзе ${chimpanzee.satiety}")

    val gorilla = Gorilla("Deek", 2, 300)
    gorilla.eat("plants")
    println("Сытость гориллы ${gorilla.satiety}")

    // Задание 2
    val animals = arrayOf(
        Lion("Alex", 1, 190),
        Tiger("Rex", 1, 150),
        Hippopotamus("Gloria", 2, 1400),
        Wolf("Rob", 1, 150),
        Giraffe("Melman", 6, 1200),
        Elephant("Lee", 3, 5000),
        Chimpanzee("Moris", 1, 70),
        Gorilla("Deek", 2, 300)
    )
    val foodList = arrayOf("zebra", "boar", "elk", "grass", "zebra")
    feeding(animals, foodList)
    for (animal in animals) {
        println("Сытость ${animal.name} = ${animal.satiety}")
    }
}

abstract class Animal(open val name: String, open val growth: Int, open val weight: Int) {
    abstract val preferences: Array<String>
    var satiety = 0
        private set

    fun eat(food: String) {
        if (preferences.contains(food)) {
            this.satiety++
        }
    }
}

class Lion(override val name: String, override val growth: Int, override val weight: Int) :
    Animal(name, growth, weight) {
    override val preferences = arrayOf("zebra", "wildebeest", "buffaloe")
}


class Tiger(override val name: String, override val growth: Int, override val weight: Int) :
    Animal(name, growth, weight) {
    override val preferences = arrayOf("boar", "deer", "roe")

}

class Hippopotamus(override val name: String, override val growth: Int, override val weight: Int) :
    Animal(name, growth, weight) {
    override val preferences = arrayOf("vegetables", "herbs", "cereals")
}

class Wolf(override val name: String, override val growth: Int, override val weight: Int) :
    Animal(name, growth, weight) {
    override val preferences = arrayOf("elk", "deer", "antelope")

}

class Giraffe(override val name: String, override val growth: Int, override val weight: Int) :
    Animal(name, growth, weight) {
    override val preferences = arrayOf("leaves", "fruits", "shrubs")
}

class Elephant(override val name: String, override val growth: Int, override val weight: Int) :
    Animal(name, growth, weight) {
    override val preferences = arrayOf("grass", "apple", "banana")
}

class Chimpanzee(override val name: String, override val growth: Int, override val weight: Int) :
    Animal(name, growth, weight) {
    override val preferences = arrayOf("fruits", "leaves", "insects")
}

class Gorilla(override val name: String, override val growth: Int, override val weight: Int) :
    Animal(name, growth, weight) {
    override val preferences = arrayOf("plants", "fruit", "nuts")
}

//Задание 2

fun feeding(animals: Array<Animal>, foodList: Array<String>) {
    for (animal in animals) {
        for (food in foodList) {
            animal.eat(food)
        }
    }
}