package course_kotlin.homework2

fun main() {
    val lemonade: Int = 18500
    val pinaKolada: Short = 200
    val whiskey: Byte = 50
    val fresh: Long = 3000000000
    val kola: Float = 0.5F
    val ale: Double = 0.666666667
    val something: String = "Что-то авторское!"

    println("Заказ - '$lemonade мл лимонада' готов!")
    println("Заказ - '$pinaKolada мл пина колады' готов!")
    println("Заказ - '$whiskey мл виски' готов!")
    println("Заказ - '$fresh капель фреша' готов!")
    println("Заказ - '$kola литра колы' готов!")
    println("Заказ - '$ale литра эля' готов!")
    println("Заказ - '$something' готов!")
}