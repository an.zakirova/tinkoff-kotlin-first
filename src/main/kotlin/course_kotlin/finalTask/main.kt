package course_kotlin.finalTask

fun main() {
    val game = Game()
    game.setup()
    game.start()
    game.getWinner()

}

fun clearConsole() {
    for (i in 0..50) {
        println()
    }
}

class UserField() {

    val FIELD = 4
    private val playerField = Array(FIELD) { IntArray(FIELD) { 0 } }

    private fun checkShipPosition(x: Int, y: Int, position: Int, countDesk: Int): Boolean {
        if (position == 1) {
            if (x <= FIELD - (countDesk - 1)) {
                for (i in x - 1..(x - 1 + (countDesk - 1))) {
                    if (playerField[y - 1][i] == 1) {
                        return false
                    }
                }
                return true
            } else return false
        } else {
            if (y <= FIELD - (countDesk - 1)) {
                for (i in y - 1..(y - 1 + (countDesk - 1))) {
                    if (playerField[i][x - 1] == 1) {
                        return false
                    }
                }
                return true
            } else return false
        }

    }

    private fun drawField() {
        for (array in playerField) {
            for (value in array) {
                val symbol = when (value) {
                    0 -> '*'
                    1 -> 'X'
                    else -> '*'
                }
                print(symbol)
            }
            println()
        }
    }

    fun tryCreateShip(modelShip: ModelShip): Boolean {
        val x = modelShip.x
        val y = modelShip.y
        val position = modelShip.position
        val countDesk = modelShip.countDesk
        val canPlaceShip = checkShipPosition(x, y, position, countDesk)
        if (canPlaceShip) {
            if (position == 1) {
                for (j in 1..countDesk) {
                    playerField[y - 1][(x - 1 + j - 1)] = 1
                }
            }
            if (position == 2) {
                for (k in 1..countDesk) {
                    playerField[y - 1 + k - 1][x - 1] = 1
                }
            }
            drawField()
        }
        return canPlaceShip
    }

    fun tryShot(coordinates: Pair<Int, Int>): Boolean {
        val x = coordinates.first
        val y = coordinates.second
        when (playerField[y - 1][x - 1]) {
            3, 2 -> {
                println("Вы сюда уже стреляли!")
                return false
            }

            1 -> {
                println("Вы попали!")
                playerField[y - 1][x - 1] = 3
                return true
            }

            else -> {
                println("Вы промахнулись!")
                playerField[y - 1][x - 1] = 2
                return false
            }
        }
    }

    fun hasAliveShips(): Boolean {
        for (array in playerField) {
            for (value in array) {
                if (value == 1) {
                    return true
                }
            }
        }
        return false
    }

    fun drawMapForEnemy() {
        for (array in playerField) {
            for (value in array) {
                val symbol = when (value) {
                    0 -> '*'
                    1 -> '*'
                    2 -> '0'
                    3 -> 'X'
                    else -> '*'
                }
                print(symbol)
            }
            println()
        }
    }
}

class ModelShip(val x: Int, val y: Int, val position: Int, val countDesk: Int) {

}

class User(val name: String) {

    val userField = UserField()

    fun requestShipData(countDesk: Int): ModelShip {

        while (true) {
            println("$name поставьте $countDesk -палубный корабль")
            try {
                var x = 0
                var y = 0
                var position = 0
                println("Введите координату Х от 1 до ${userField.FIELD}")
                x = readLine()!!.toInt()
                println("Введите координату Y от 1 до ${userField.FIELD}")
                y = readLine()!!.toInt()

                if (countDesk > 1) {
                    println("Позиция корабля: 1 - горизонтально, 2 - вертикально")
                    position = readLine()!!.toInt()

                } else {
                    position = 1
                }
                if ((x in 1..userField.FIELD) && (y in 1..userField.FIELD) && (position in 1..2)) {

                    return ModelShip(x, y, position, countDesk)
                } else {
                    println("Введите корректные данные")
                }

            } catch (exp: NumberFormatException) {
                println("Ошибка")

            }
        }
    }

    fun requestShot(): Pair<Int, Int> {

        while (true) {
            try {

                println("Введите координату Х от 1 до 4")
                val x = readLine()!!.toInt()
                println("Введите координату Y от 1 до 4")
                val y = readLine()!!.toInt()

                if ((x in 1..userField.FIELD) && (y in 1..userField.FIELD)) {
                    return Pair(x, y)
                } else {
                    println("Введите корректные данные")
                }
            } catch (exp: Exception) {
                println("Ошибка")
            }
        }
    }
}

class Game() {
    private var user1: User? = null
    private var user2: User? = null

    private val maxShipDesk = 2
    private var winner: User? = null

    fun setup() {

        user1 = setupUser(1)
        user2 = setupUser(2)
        fillMapForUser(user1!!)
        clearConsole()
        fillMapForUser(user2!!)
        clearConsole()
        winner = null
    }

    fun start() {
        if (user1 == null || user2 == null) {
            println("Проинициализируйте игру")
            return
        }
        do {
            if (makeTurn(user1!!, user2!!)) {
                winner = user1
                break
            }
            if (makeTurn(user2!!, user1!!)) {
                winner = user2
                break
            }

        } while (true)
        println("Игра окончена \n")

    }

    fun getWinner() {
        if (winner != null) {
            println("Победитель ${winner?.name}")
        }
    }

    private fun setupUser(userNumber: Int): User {
        println("Игрок $userNumber введите имя")
        val nameUser = readLine() ?: " Введите корректное имя"
        println("Добро пожаловать в игру $nameUser")
        return User(nameUser)
    }

    private fun fillMapForUser(user: User) {
        for (countDesk in maxShipDesk downTo 1) {
            var countShip = 1
            while (countShip <= (maxShipDesk - countDesk + 1)) {
                val modelShip = user.requestShipData(countDesk)
                if (user.userField.tryCreateShip(modelShip)) {
                    countShip++
                    println("Корабль поставлен")
                } else {
                    println("Невозможно поставить корабль")
                }

            }
        }
    }

    private fun makeTurn(userFrom: User, userTo: User): Boolean {
        while (true) {
            println("${userFrom.name} сделайте ваш ход")
            userTo.userField.drawMapForEnemy()
            val coordinatesShot = userFrom.requestShot()
            val isSuccess = userTo.userField.tryShot(coordinatesShot)
            if (isSuccess) {
                if (!userTo.userField.hasAliveShips()) {
                    return true
                }
            } else {
                return false
            }
        }
    }
}



