package course_kotlin.homework7

fun main() {
    val login = readLine() ?: ""
    val password = readLine() ?: ""
    val password2 = readLine() ?: ""
    println(authentication(login, password, password2))
}

fun authentication(login: String, password: String, password2: String): User {
    if (login.length > 20) {
        throw WrongLoginException()
    }
    if (password.length < 10) {
        throw WrongPasswordException("Длина пароля должна быть больше 10 символов")
    }
    if (password != password2) {
        throw WrongPasswordException("Пароли должны совпадать")
    }
    return User(login, password)
}

class WrongLoginException : Exception("Длина логина должна быть не больше 20 символов") {

}

class WrongPasswordException(messageEx: String) : Exception(messageEx)

data class User(val login: String, val password: String)

