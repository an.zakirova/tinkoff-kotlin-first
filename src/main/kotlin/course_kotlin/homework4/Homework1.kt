package course_kotlin.homework4

fun main() {
    val myArray = arrayOf(1, 25, 10, 6, 22, 17, 8, 14, 23)
    var capitan = 0
    var team = 0
    for (i in 0..myArray.size - 1) {
        if (myArray[i] % 2 == 0) {
            capitan++
        } else team++
    }
    println("У Джо $capitan монет")
    println("У команды $team монеты")
}
