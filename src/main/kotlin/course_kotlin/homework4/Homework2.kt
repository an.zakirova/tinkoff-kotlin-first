package course_kotlin.homework4

fun main() {
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)
    var exel = 0.0
    var good = 0.0
    var bad = 0.0
    var veryBad = 0.0
    for (i in marks) {
        when (i) {
            2 -> veryBad++
            3 -> bad++
            4 -> good++
            5 -> exel++
        }
    }
    exel = exel * 100 / marks.size
    good = good * 100 / marks.size
    bad = bad * 100 / marks.size
    veryBad = veryBad * 100 / marks.size

    println("Отличников - " + String.format("%.1f", exel) + "%")
    println("Ударников - " + String.format("%.1f", good) + "%")
    println("Троечников - " + String.format("%.1f", bad) + "%")
    println("Двоечников - " + String.format("%.1f", veryBad) + "%")
}