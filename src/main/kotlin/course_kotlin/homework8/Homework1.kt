package course_kotlin.homework8

fun main() {
    val sourceList = mutableListOf(1, 2, 3, 1, 2, 3, 1, 1, 5, 6, 8, 1, 8, 7, 4, 9)
    val list = EliminationOfDuplicates().conversionList(sourceList)
    println(list)
}

class EliminationOfDuplicates() {
    fun conversionList(list: List<Int>): List<Int> {
        val toSet = list.toSet()
        return toSet.toList()
    }
}

