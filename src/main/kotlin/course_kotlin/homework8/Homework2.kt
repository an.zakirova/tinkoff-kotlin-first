package course_kotlin.homework8

val userCart = mutableMapOf<String, Int>(
    "potato" to 2,
    "cereal" to 2,
    "milk" to 1,
    "sugar" to 3,
    "onion" to 1,
    "tomato" to 2,
    "cucumber" to 2,
    "bread" to 3
)
val discountSet = setOf("milk", "bread", "sugar")
val discountValue = 0.20
val vegetableSet = setOf("potato", "tomato", "onion", "cucumber")
val prices = mutableMapOf<String, Double>(
    "potato" to 33.0,
    "sugar" to 67.5,
    "milk" to 58.7,
    "cereal" to 78.4,
    "onion" to 23.76,
    "tomato" to 88.0,
    "cucumber" to 68.4,
    "bread" to 22.0
)

fun main() {
    countVegetable(userCart, vegetableSet)
    countPrice(userCart, prices, discountSet, discountValue)
}

fun countVegetable(userCart: Map<String, Int>, vegetableSet: Set<String>) {
    var countVegetable = 0
    for ((key, value) in userCart) {
        if (vegetableSet.contains(key)) {
            countVegetable += value
        }
    }
    println("Овощей в корзине - $countVegetable")
}

fun countPrice(
    userCart: Map<String, Int>,
    prices: Map<String, Double>,
    discountSet: Set<String>,
    discountValue: Double
) {
    val listProductsWithPrice: MutableMap<String, Double> = mutableMapOf()
    for (key in userCart.keys) {
        var valuePrice = userCart.getValue(key) * prices.getValue(key)
        if (discountSet.contains(key)) {
            valuePrice *= (1 - discountValue)
        }
        listProductsWithPrice[key] = valuePrice
    }
    val sumProducts = listProductsWithPrice.values.sum()
    println("Сумма товаров в корзине = $sumProducts")
}



