package course_kotlin.homework5

fun main() {
    val lion = Lion("Alex", 1, 190)
    lion.eat("zebra")
    println(lion.satiety)

    val tiger = Tiger("Rex", 1, 150)
    tiger.eat("boar")
    println(tiger.satiety)

    val hippopotamus = Hippopotamus("Gloria", 2, 1400)
    hippopotamus.eat("vegetables")
    println(hippopotamus.satiety)

    val wolf = Wolf("Rob", 1, 150)
    wolf.eat("elk")
    println(wolf.satiety)

    val giraffe = Giraffe("Melman", 6, 1200)
    giraffe.eat("leaves")
    println(giraffe.satiety)

    val elephant = Elephant("Lee", 3, 5000)
    elephant.eat("grass")
    println(elephant.satiety)

    val chimpanzee = Chimpanzee("Moris", 1, 70)
    chimpanzee.eat("fruits")
    println(chimpanzee.satiety)

    val gorilla = Gorilla("Deek", 2, 300)
    gorilla.eat("plants")
    println(gorilla.satiety)
}

class Lion(val name: String, val growth: Int, val weight: Int) {
    private val preferences = arrayOf("zebra", "wildebeest", "buffaloe")
    var satiety = 0
        private set

    fun eat(food: String) {
        if (preferences.contains(food)) {
            this.satiety++
        }
    }
}

class Tiger(val name: String, val growth: Int, val weight: Int) {
    private val preferences = arrayOf("boar", "deer", "roe")
    var satiety = 0
        private set

    fun eat(food: String) {
        if (preferences.contains(food)) {
            this.satiety++
        }
    }
}

class Hippopotamus(val name: String, val growth: Int, val weight: Int) {
    private val preferences = arrayOf("vegetables", "herbs", "cereals")
    var satiety = 0
        private set

    fun eat(food: String) {
        if (preferences.contains(food)) {
            this.satiety++
        }
    }
}

class Wolf(val name: String, val growth: Int, val weight: Int) {
    private val preferences = arrayOf("elk", "deer", "antelope")
    var satiety = 0
        private set

    fun eat(food: String) {
        if (preferences.contains(food)) {
            this.satiety++
        }
    }
}

class Giraffe(val name: String, val growth: Int, val weight: Int) {
    private val preferences = arrayOf("leaves", "fruits", "shrubs")
    var satiety = 0
        private set

    fun eat(food: String) {
        if (preferences.contains(food)) {
            this.satiety++
        }
    }
}

class Elephant(val name: String, val growth: Int, val weight: Int) {
    private val preferences = arrayOf("grass", "apple", "banana")
    var satiety = 0
        private set

    fun eat(food: String) {
        if (preferences.contains(food)) {
            this.satiety++
        }
    }
}

class Chimpanzee(val name: String, val growth: Int, val weight: Int) {
    private val preferences = arrayOf("fruits", "leaves", "insects")
    var satiety = 0
        private set

    fun eat(food: String) {
        if (preferences.contains(food)) {
            this.satiety++
        }
    }
}

class Gorilla(val name: String, val growth: Int, val weight: Int) {
    private val preferences = arrayOf("plants", "fruit", "nuts")
    var satiety = 0
        private set

    fun eat(food: String) {
        if (preferences.contains(food)) {
            this.satiety++
        }
    }
}

