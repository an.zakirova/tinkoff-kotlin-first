package course_kotlin.homework5

fun main() {
    val digit = readLine()!!.toInt()
    val result = InverseNumber()
    result.inverse(digit)
}

class InverseNumber() {
    fun inverse(value: Int) {
        val sign: Int
        if (value < 0) {
            sign = -1
        } else sign = 1

        var valueNew = value * sign
        val buffer = mutableListOf<Int>()
        while (valueNew != 0) {
            val element = valueNew % 10
            buffer.add(element)
            valueNew /= 10
        }
        val numberString = buffer.joinToString("")
        val numberInt = numberString.toInt() * sign
        println(numberInt)
    }


}