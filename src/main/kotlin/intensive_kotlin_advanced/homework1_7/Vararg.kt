package intensive_kotlin_advanced.homework1_7

fun main() {
    varargInput("12", "122", "1234", "fpo")
}

fun varargInput(vararg params: String) {

    val count = params.count()
    println("Переданно $count элемента:")
    params.forEach {
        print("$it;")
    }
}