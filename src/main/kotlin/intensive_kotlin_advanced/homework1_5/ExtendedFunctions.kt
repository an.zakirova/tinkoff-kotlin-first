package intensive_kotlin_advanced.homework1_5

fun main() {
    configuresPerson(
        name = "Вася",
        surname = "Петров",
        patronymic = "Иванович",
        gender = "Male",
        age = 44,
        dateOfBirth = "10-09-1997",
        inn = 777736663,
        snils = "111-111-111"
    )
    configuresPerson(name = "Петя", surname = "Иванов", gender = "Male", age = 44, dateOfBirth = "10-09-1997")
    configuresPerson(
        surname = "Иванов",
        gender = "Male",
        name = "Костя",
        age = 44,
        inn = 777736663,
        snils = "111-111-111",
        dateOfBirth = "10-09-1997"
    )
}

fun configuresPerson(
    name: String,
    surname: String,
    patronymic: String? = null,
    gender: String,
    age: Int,
    dateOfBirth: String,
    inn: Int? = null,
    snils: String? = null
) {
    val result = listOfNotNull(name, surname, patronymic, gender, age, dateOfBirth, inn, snils)
    println(result)
}