package intensive_kotlin_advanced.homework1_13

import java.time.LocalDate

fun main() {

    definitionType("gfgfg")
    definitionType(123)
    definitionType(202.3345)
    definitionType(LocalDate.of(1990, 12, 29))
    definitionType(null)
    definitionType(emptyArray<Int>())
}

fun definitionType(arg: Any?) {
    when (arg) {
        is String -> {
            println("Я получил тип String = '$arg' ее длина равна ${arg.length} символ")
        }

        is Int -> {
            println("Я получил Int = $arg, его квадрат равен ${arg * arg}")
        }

        is Double -> {
            println("Я получил Double = $arg, это число округляется до ${"%.2f".format(arg)}")
        }

        null -> {
            println("Мы получили null")
        }

        is LocalDate -> {
            val dateTinkoff = LocalDate.of(2006, 12, 24)
            when {
                arg.isAfter(dateTinkoff) -> println("Я получил LocalData = $arg, эта дата позже, чем дата основания Tinkoff")
                arg.isBefore(dateTinkoff) -> println("Я получил LocalData = $arg, эта дата раньше, чем дата основания Tinkoff")
                arg.isEqual(dateTinkoff) -> println("Я получил LocalData = $arg, эта дата равна дате основания Tinkoff")
            }
        }

        else -> println("${arg.javaClass.simpleName} Мне этот тип неизвестен")
    }
}