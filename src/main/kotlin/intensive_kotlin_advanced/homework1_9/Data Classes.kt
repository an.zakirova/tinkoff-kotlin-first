package intensive_kotlin_advanced.homework1_9

import java.time.LocalDate

fun main() {

    val person1 = configuresPerson("Vasya", "Petrov", null, "male", 14, LocalDate.of(1999, 10, 12), null, null)
    println(person1)
}

data class Person(
    val name: String,
    val surname: String,
    val patronymic: String? = null,
    val gender: String,
    val age: Int,
    val dateOfBirth: LocalDate,
    val inn: Int? = null,
    val snils: String? = null
)

fun configuresPerson(
    name: String,
    surname: String,
    patronymic: String? = null,
    gender: String,
    age: Int,
    dateOfBirth: LocalDate,
    inn: Int? = null,
    snils: String? = null
): Person {
    return Person(name, surname, patronymic, gender, age, dateOfBirth, inn, snils)
}