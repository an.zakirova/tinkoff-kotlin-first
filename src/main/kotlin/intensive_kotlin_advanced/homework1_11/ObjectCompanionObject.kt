package intensive_kotlin_advanced.homework1_11

fun main() {
    Counter.counter()
    Counter.counter()
}

class Counter() {
    companion object MyCompanion {
        private var count = 1
        fun counter() {
            println("Вызван counter. Количество вызовов = $count")
            count++
        }
    }
}
