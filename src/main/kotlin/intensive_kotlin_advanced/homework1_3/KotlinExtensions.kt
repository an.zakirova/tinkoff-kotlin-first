package intensive_kotlin_advanced.homework1_3

fun MutableList<Int>.square() {
    for (index in 0 until size) {
        val element = this[index]
        set(index, element * element)
    }
}

fun main() {
    val list = mutableListOf(1, 4, 9, 16, 25)
    list.square()
    println(list)

}
